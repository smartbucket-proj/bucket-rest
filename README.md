# REST API сервис

Сервис предоставляет основные методы для работы с базой данных, а так же должен будет обеспечивать генерацию и обработку токенов и авторизированный доступ.
## Основные точки доступа
1. `[ipaddress]:[port]/Product` - Репозиторий продуктов
2. `[ipaddress]:[port]/Basket` - Репозиторий корзинок

##  Поддерживаемы возможности
Доступные методы для репозиториев: GET, POST, DELETE. \
Доступные методы для элементов репозиториев: GET, PATCH, PUT, DELETE.

1. Post для Product
```
{
	"name" : "Товар",
	"description" : "Описание",
	"count" : 5,
	"calorie" : 145.5,
	"price" : 89.5,
	"imageLink" : "http://#"
}
```
2. Post для Basket
```
{
	"token" : 123654,
	"tokenTTL" : 360,
	"products" : [1]
}
```
3. Имеется связь между корзинкой и продуктами в ней. \
Благодарая этому, можно увидеть список продуктов в корзинке простым GET \
`[ipaddress]:[port]/Basket/1`
```
{
    "products": [
        1
    ],
    "id": 1,
    "token": 123654,
    "tokenTTL": 360,
    "_updated": "Tue, 21 Apr 2020 10:30:42 GMT",
    "_created": "Tue, 21 Apr 2020 10:30:42 GMT",
    "_etag": "34e56a7add0bc1e9a2fedf6e3f2a0b61457f55a1",
    "_links": {
        "parent": {
            "title": "home",
            "href": "/"
        },
        "self": {
            "title": "Basket",
            "href": "Basket/1"
        },
        "collection": {
            "title": "Basket",
            "href": "Basket"
        }
    }
}
```
4. Поддерживается поиск в репозиториях: \
`[ipaddress]:[port]/Product?where={"name": "Товар"}`

5. Поддерживается постраничный вывод: \
`[ipaddress]:[port]/Product?max_results=20&page=1`