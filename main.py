from eve import Eve
from sqlalchemy import Table, Column, Integer, String, Float, ForeignKey, DateTime, func
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import column_property, relationship
from sqlalchemy.orm import validates
from eve_sqlalchemy import SQL
from eve_sqlalchemy.config import DomainConfig, ResourceConfig
from eve_sqlalchemy.validation import ValidatorSQL
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
from itsdangerous import SignatureExpired, BadSignature
from werkzeug.security import generate_password_hash, check_password_hash
from werkzeug.exceptions import Unauthorized
from eve.auth import TokenAuth
from flask import request
from flask import request, jsonify
import socketio
import random
import sys
import hashlib
import string
import random
import json
import base64
import datetime

from sqlalchemy.ext.declarative import DeclarativeMeta

sio = socketio.Client()

#Сериализация в объектов JSON 
class AlchemyEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj.__class__, DeclarativeMeta):
            # an SQLAlchemy class
            fields = {}
            for field in [x for x in dir(obj) if not x.startswith('_') and x != 'metadata']:
                data = obj.__getattribute__(field)
                try:
                    json.dumps(data) # this will fail on non-encodable values, like other classes
                    fields[field] = data
                except TypeError:
                    fields[field] = None
            # a json-encodable dict
            return fields

        return json.JSONEncoder.default(self, obj)

SECRET_KEY = 'this-is-my-super-secret-key'
#https://eve-sqlalchemy.readthedocs.io/en/latest/tutorial.html
#https://docs.python-eve.org/en/stable/features.html
#https://docs.sqlalchemy.org/en/13/orm/basic_relationships.html
Base = declarative_base()

#Класс описывающий метаданные сущностей
class CommonColumns(Base):
    __abstract__ = True
    _created = Column(DateTime, default=func.now())
    _updated = Column(DateTime, default=func.now(), onupdate=func.now())

#Структура для связи корзинки и продуктов
class ProductInBasket(CommonColumns):
    __tablename__ = 'ProductInBasket'
    id = Column('id', Integer, primary_key=True, autoincrement=True)
    Bid = Column('Bid', Integer)
    Pid = Column('Pid', Integer)
    Count = Column(Integer)


class Basket(CommonColumns):
    __tablename__ = 'Basket'
    id = Column(Integer, primary_key=True)
    board_name = Column(String)
    ip = Column(String)
    token = Column(Integer)
    mobile = Column(String)
    password = Column(String)
    status = False

    #roles = relationship("Role", backref="users")

    def generate_auth_token(self, expiration=60*60):
        """Генерация токена для корзинки."""
        s = Serializer(SECRET_KEY, expires_in=expiration)
        return s.dumps({'id': self.id })

    @staticmethod
    def verify_auth_token(token):
        """Проверяем токен и возвращаем id корзинки
        """
        s = Serializer(SECRET_KEY)
        try:
            data = s.loads(token)
        except SignatureExpired:
            return None # время жизни токена истекло
        except BadSignature:
            return None # не корректный токен
        return data['id']

    #def isAuthorized(self, role_names):
    def isAuthorized(self):
        """Дополнительный метод, можно добавить проверку ролей
        """
        #allowed_roles = set([r.id for r in self.roles])\
        #    .intersection(set(role_names))
        # return len(allowed_roles) > 0
        return True

    def encrypt(self, password):
        """Хешируем пароль с помощью werkzeug security module.
        """
        return generate_password_hash(password)

    @validates('password')
    def _set_password(self, key, value):
        """Проверяем что при добавлении новый пароль будет хеширован.
        """
        return self.encrypt(value)

    def check_password(self, password):
        if not self.password:
            return False
        return check_password_hash(self.password, password)

    def __str__(self):
        return f'{self.id} {self.ip} {self.token} {self.mobile}'

    @property
    def serialize(self):
        return {
            'id': self.id,
            'ip': self.ip,
            'name': self.board_name,
            'status': self.status
        }
    
# Класс описывает продукт в магазине       
class Product(CommonColumns):
    __tablename__ = 'Product'
    id = Column(String(60), primary_key=True)
    name = Column(String(120))
    description = Column(String(2500))
    count = Column(Integer)
    calorie = Column(Float)
    price = Column(Float)
    imageLink = Column(String(1000))

    @property
    def serialize(self):
        return {
            'id': self.id,
            'name': self.name,
            'description': self.description,
            'count': 1,
            'calories': self.calorie,
            'price': self.price,
            'img_url': self.imageLink
        }


# Авторизация на основе базовой токен авторизации
class MarketTokenAuth(TokenAuth):
    def check_auth(self, token, allowed_roles, resource, method):
        """Проверяем что токен действителен 
        """
        if token == "admin":
            return True
        login = Basket.verify_auth_token(token)
        if login:
            return True
        else:
            return False
def check_auth(token):
    """Проверяем что токен действителен 
    """
    if token == "admin":
        return True
    login = Basket.verify_auth_token(token)
    if login:
        return True
    else:
        return False


# Основные настройки приложения
SETTINGS = {
    'ITEM_METHODS' : ['GET', 'PATCH', 'PUT', 'DELETE'],
    'RESOURCE_METHODS' : ['GET', 'POST'],
    'IF_MATCH' : False,
    'BANDWIDTH_SAVER' : False,
    'DEBUG': True,
    'SQLALCHEMY_DATABASE_URI': 'postgresql://postgres:postgres@localhost:5432/Market',
    'SQLALCHEMY_TRACK_MODIFICATIONS': False,
    'DOMAIN': DomainConfig({
        'Basket': ResourceConfig(Basket),
        'Product': ResourceConfig(Product),
        'ProductInBasket' : ResourceConfig(ProductInBasket)
    }).render()
}

# Присваем настройки
app = Eve(auth=MarketTokenAuth, settings=SETTINGS, validator=ValidatorSQL, data=SQL)

# Подключаем подержку SQLAlchemy
db = app.data.driver
Base.metadata.bind = db.engine
db.Model = Base
db.create_all()

#
#  Раздел  энд-поинтов 
#

@app.route('/health/buckets', methods=['GET'])
def buckets():
    all_buckets = db.session.query(Basket).all()
    print(all_buckets[0].ip)

    for i in all_buckets:
        try:
            i.status = True
            if(i.id != 1):
                sio.connect(f'{i.ip}:5000')
        except socketio.exceptions.ConnectionError:
            i.status = False

    return jsonify([i.serialize for i in all_buckets])

@app.route('/health/ping', methods=['GET'])
def ping():
    """Проверяем доступность сервиса"""
    return jsonify({"it's work fine" : True,
                    "time is" : datetime.datetime.now()})

@app.route('/product/getById', methods=['GET'])
def getProductById(**kwargs):
    rfidId = str(request.args.get('id'))
    print(f'Gonna fetch product: {rfidId}')
    product = db.session.query(Product).get(rfidId)

    return json.dumps(product, cls=AlchemyEncoder)

@app.route('/mobile/connectToBasket', methods=['GET'])
def connectToBasket():
    """Подключение мобильного приложения к корзинке"""
    basketToken = int(request.args.get('token'))
    mobileId = str(request.args.get('mobile'))
    if db.session.query(Basket).filter(Basket.mobile == mobileId, Basket.token == basketToken).count() == 0:
        db.session.query(Basket).filter(Basket.token == basketToken).update({"mobile": mobileId}, synchronize_session='evaluate')
        token = db.session.query(Basket).filter(Basket.token == basketToken).first()
        token = db.session.query(Basket).get(token.id)
        return jsonify({'token': (token.generate_auth_token()).decode('ascii'), 'BascketId' : token.id})

@app.route('/mobile/closeConnect')
def closeConnect():
    """Отключение мобильного приложения от корзинки"""
    basketToken = int(request.args.get('token'))
    mobileId = int(request.args.get('mobile'))
    db.session.query(Basket).filter(Basket.mobile == mobileId, Basket.token == basketToken).update({"mobile": None}, synchronize_session='evaluate')
    return "OK"

@app.route('/login', methods=['GET'])
def login(**kwargs):
    """Метод авторизации корзинки,выдает токен доступа и
     токен для мобильного приложения
    """
    
    basketId = request.args.get('id')
    password = request.args.get('password')

    if not login or not password:
        raise Unauthorized('Не верный логин или пароль.')
    else:
        basket = db.session.query(Basket).get(basketId)
        if basket and basket.check_password(password):
            newToken = str(random.randrange(10)) + str(random.randrange(10)) + str(random.randrange(10)) +str(random.randrange(10))
            while db.session.query(Basket).filter(Basket.token == newToken).count() > 0:
                newToken = str(random.randrange(10)) + str(random.randrange(10)) + str(random.randrange(10)) +str(random.randrange(10))
            if db.session.query(Basket).filter(Basket.id == basketId).count() > 0:
                db.session.query(Basket).filter(Basket.id == basketId).update({"token": newToken}, synchronize_session='evaluate')
                db.session.commit()
            token = basket.generate_auth_token()
            return jsonify({'token': token.decode('ascii'), 'mobileToken' : newToken})
    raise Unauthorized('Не верный логин или пароль.')

# @app.route('/userLogin', methods=['GET'])
# def user_login(**kwargs):    
#     login = request.args.get('login')
#     password = request.args.get('password')

#     if not login or not password:
#         raise Unauthorized('Не верный логин или пароль.')
#     else:
#         user = db.session.query(User).get(login)
#         if user and user.check_password(password):
#             return jsonify({'UserAuthorised': True})
#     raise Unauthorized('Не верный логин или пароль.')

#Получение товаров в корзинке
@app.route('/GetProductsInBasket', methods=['GET'])
def GetProductsInBasket():
    token = request.headers.get(key="Authorization")
    if check_auth(token):
        basketId = request.args.get('id')
        productsIds = db.session.query(ProductInBasket.Pid).filter(ProductInBasket.Bid == basketId).all()
        Pids = []       
        for Pid in productsIds:
            Pids.append(Pid.Pid)
        ProductsIn = db.session.query(Product).filter(Product.id.in_(Pids)).all()
        return json.dumps(ProductsIn, cls=AlchemyEncoder)
    else:
        raise Unauthorized('Не верный логин или пароль.')

@app.route('/out/printCheck')
def print_check(**kwargs):
    products = request.args.get('products')
    total_price = 0
    for p in products:
        product_price = p.price * p.count
        total_price += produt_price
        print(f'{p.name}     {p.count}x{p.price} = {product_price}')
    print(f'Итого: {product_price}')

# Добавляем CORS
@app.after_request
def after_request(response):
    response.headers.set('Access-Control-Allow-Origin', '*')
    response.headers.set('Access-Control-Allow-Headers', 'Content-Type, Authorization')
    #response.headers.set('Access-Control-Allow-Methods', 'GET, POST, PUT')
    return response

# Обработка параметров запуска
use_reloader_state = False
if len (sys.argv) > 1:
    if sys.argv[1] == 'True' or sys.argv[1] == 'False':
        print("use_reloader = {}".format(sys.argv[1]))
        use_reloader_state = bool(sys.argv[1])
else:
    print(f"use_reloader = {use_reloader_state}")
app.run(host='0.0.0.0', debug=True, use_reloader=use_reloader_state, port=5001)
